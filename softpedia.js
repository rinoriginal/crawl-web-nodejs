const cheerio = require('cheerio')
const axios = require('axios')
const fs = require('fs')
const website = 'https://www.softpedia.com'
const news = ' h2.idxlb_1 > a:nth-child(1)'
const latest = '#sjmp > div.grid_32 > h2 > a:nth-child(1)'
const arraylist = [news]
const last = 'div.cls_pagination > div > a:nth-child(11)'
                
const arraylatest = [last]

async function GetDataSoftpedia(){
    await axios.get(website).then(async(urlResponse) =>{
        const $ = cheerio.load(urlResponse.data)
        const array = $('#wrapper > div.menubar-hp > div.container_48 > div > div.navtopm > ul.lev1.navsect.locmenu > li > a')
        for await (let index of array) {
            const Web = index.attribs.href
            const re = Web.replace('https://','')
            const po = re.indexOf('.')
            const re1 = re.slice(po)
            const aliasWeb = re.replace(re1,'') // win drivers games mac mobile linux
            // console.log(aliasWeb)
            await axios.get(Web).then(async(urlRes) =>{
                const $ = cheerio.load(urlRes.data)
                for await (let index1 of arraylist){    
                    const array1 = $(index1)
                    for await (let index2 of array1){
                        let link = index2.attribs.href
                        const realink = link.replace('https://','')
                        const po1 = realink.indexOf('.')
                        const re2 = realink.slice(po1)
                        const  aliaslink = realink.replace(re2,'')
                        // console.log(aliaslink)
                        await axios.get(link).then(async(urllink) =>{
                            const $ = cheerio.load(urllink.data)
                            const nlink = $('div.grid_38.cls_pagination > div.fr.ta_right > a.smaller')
                            for await (let index3 of nlink){
                                const alink = index3.attribs.href
                                const n = $(index3).text()
                                const n1 = n.replace(',','')
                                const relink = alink.replace(`${n1}`,' ')
                                for(i=1;i<=n1;i++){
                                    if(i==1){
                                        linkdb = link
                                    }else{
                                        linkdb = relink.replace(' ',i)
                                    }
                                    console.log(linkdb)
                                    await axios.get(linkdb).then(async(urllinkdb) => {
                                        const $ = cheerio.load(urllinkdb.data)
                                        const array4 = $('#wrapper > div.main > div.container_48 > div > h3 > a')
                                        for await (let index4 of array4){
                                            const linkcategory = index4.attribs.href
                                            const position = linkcategory.lastIndexOf('/')
                                            const b = linkcategory.slice(position+1)
                                            const position1 = b.lastIndexOf('.')
                                            const c = b.slice(position1)
                                            const alias = b.replace(c,'')
                                            await axios.get(linkcategory).then(async(urlCate) =>{
                                                const $ = cheerio.load(urlCate.data)
                                                const dataweb = $.html()
                                                if (fs.existsSync(`./${aliasWeb}/${aliaslink}/${alias}.html`)){
                                                    console.log(`ton tai ${aliasWeb}/${aliaslink}/${alias}.html`,i)
                                                }
                                                else{
                                                    fs.writeFile(`./${aliasWeb}/${aliaslink}/${alias}.html`, dataweb, { flag: 'a+' }, err => {
                                                        if (err)
                                                            console.log("Error")
                                                        else {
                                                            console.log(`${alias}`)
                                                        }
                                                    })
                                                }
                                            }).catch(()=>{})
                                        }
                                    }).catch(()=>{})
                                }
                                
                            }
                        }).catch(()=>{})
                    }
                }
                for await (let index6 of arraylatest){
                    const xarray = $(index6)
                    const array6 = $(index6).text()
                    const nn = array6.replace(',','') //30 30 30 30 4055 30
                    if(aliasWeb =='mobile'){
                        for await(let index7 of xarray){
                            const array7 = index7.attribs.href
                            const relink2 = array7.replace(nn,' ')
                            for(j=1;j<=nn;j++){
                                if(j==1){
                                    links = Web
                                }
                                else{
                                    links = relink2.replace(' ',j)
                                }
                                await axios.get(links).then(async(urllinks)=>{
                                    const $ = cheerio.load(urllinks.data)
                                    const db = $('div.dlcls > h4.ln > a')
                                    const tg = db.text()
                                    if(tg==''){
                                        db = $('div.grid_48 > div.top > h3 > a')
                                    }
                                    for await(let index8 of db){
                                        const fulllink= index8.attribs.href
                                        const polink1 = fulllink.replace('https://','') 
                                        const polink = polink1.indexOf('/')
                                        const pofull = polink1.slice(polink+1)
                                        const polink2 = pofull.indexOf('/')
                                        const pofull1 = pofull.slice(polink2+1)
                                        const poa = pofull1.slice(0,-1)
                                        const poafull = poa.replace('/','-')
                                        const aliasl = poafull.replace('/','-')
                                        
                                        await axios.get(fulllink).then(async(urlCate1) =>{
                                            const $ = cheerio.load(urlCate1.data)
                                            const dataweb1 = $.html()
                                            if (fs.existsSync(`./${aliasWeb}/${aliasl}.html`)){
                                                console.log(`ton tai ${aliasWeb}/${aliasl}.html`,i)
                                            }else{
                                                fs.writeFile(`./${aliasWeb}/${aliasl}.html`, dataweb1, { flag: 'a+' }, err => {
                                                    if (err)
                                                        console.log("Error")
                                                    else {
                                                        console.log(`${aliasl}`,`${aliasWeb}`)
                                                    }
                                                })
                                            }
                                        }).catch(()=>{})
                                    }
                                }).catch(()=>{})
                            }
                        }
                    }
                    else{
                        for await(let index7 of xarray){
                            const array7 = index7.attribs.href
                            const relink2 = array7.replace(nn,' ')
                            for(j=1;j<=nn;j++){
                                if(j==1){
                                    links = Web
                                }
                                else{
                                    links = relink2.replace(' ',j)
                                }
                                await axios.get(links).then(async(urllinks)=>{
                                    const $ = cheerio.load(urllinks.data)
                                    const db = $('#sjmp > div.grid_48 > h4 > a')
                                    const tg = db.text()
                                    if(tg==''){
                                        db = $('#sjmp > div.grid_48 > div.top > h3 > a')
                                    }
                                    for await(let index8 of db){
                                        const fulllink= index8.attribs.href
                                        const polink = fulllink.lastIndexOf('/') 
                                        const pofull = fulllink.slice(polink+1)
                                        const poa = pofull.indexOf('.')
                                        const poafull = pofull.slice(poa)
                                        const aliasl = pofull.replace(poafull,'')
                                        await axios.get(fulllink).then(async(urlCate1) =>{
                                            const $ = cheerio.load(urlCate1.data)
                                            const dataweb1 = $.html()
                                            if (fs.existsSync(`./${aliasWeb}/${aliasl}.html`)){
                                                console.log(`ton tai ${aliasWeb}/${aliasl}.html`)
                                            }else{
                                                fs.writeFile(`./${aliasWeb}/${aliasl}.html`, dataweb1, { flag: 'a+' }, err => {
                                                    if (err)
                                                        console.log("Error")
                                                    else {
                                                        console.log(`${aliasl}`)
                                                    }
                                                })
                                            }
                                        }).catch(()=>{})
                                    }
                                }).catch(()=>{})
                            }
                        }
                    }
                }

            }).catch(()=>{})
        }
    }).catch(()=>{})
}


GetDataSoftpedia()


