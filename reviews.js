const fs =  require('fs')
const puppeteer = require('puppeteer')
const linkwebsite = 'https://www.reviews.io'
const website = 'https://www.reviews.io/front/integration-library/'

async function getdata(){
    let browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null
    })
    let page = await browser.newPage()

    await page.goto(website, {waitUntil: 'networkidle2'})
    await page.waitForTimeout(2000) 
    await page.evaluate('window.scrollTo(0, document.body.scrollHeight)')
    await page.waitForTimeout(2000) 
    await page.waitForSelector('div.list')
    await page.waitForTimeout(2000) 
    const result1s = await page.$$eval('div.list > a', row1s => {
        return row1s.map(row1 => {
            const properties1 = {}
            properties1.url = row1.getAttribute('href')
            return properties1
        })
    })
    
    // xu ly alias va luu file
    for await (let index of result1s){
        const fulllink = linkwebsite.concat(index.url)
        const po = fulllink.lastIndexOf('/')
        const alias = fulllink.slice(po+1)
        const browser3 = await puppeteer.launch();
        try {
            const page = await browser3.newPage();
            await page.goto(`${fulllink}`);
            await page.waitForTimeout(2000);
            let html_content = await page.evaluate(el => el.innerHTML, await page.$('#__nuxt'));
            if (fs.existsSync(`./data/${alias}.html`)){
                console.log(`ton tai data/${alias}.html`)
            }
            else{
                fs.writeFile(`./data/${alias}.html`, html_content, { flag: 'a+' }, err => {
                    if (err)
                        console.log("Error")
                    else {
                        console.log(`${alias}`)
                    }
                })
            }
            
        } catch (err) {
            console.log(err);
        }
        await browser3.close()
    }
    // dong xu ly alias va luu file

    // xu ly link
    const result = await page.$eval('#__layout > div > div:nth-child(4) > div > div > div.f-center > div:nth-child(3) > div:nth-child(1) > a:nth-child(5)', el => el.href)
    const po1 = result.lastIndexOf('/')
    const re1 = result.slice(po1)
    const linkagency = result.replace(re1,'')
    // dong xu ly link 
    let browser1 = await puppeteer.launch()
    let page1 = await browser1.newPage()

    await page1.goto(result, {waitUntil: 'networkidle2'})
    await page1.waitForTimeout(4000)
    await page1.evaluate('window.scrollTo(0, document.body.scrollHeight)')
    await page1.waitForTimeout(8000) 
    await page1.waitForSelector('div.list')
    await page1.waitForTimeout(6000) 
    const data = await page1.$$eval('div.list > a', row1s => {
        return row1s.map(row1 => {
            const properties1 = {}
            properties1.url = row1.getAttribute('href')
            return properties1
        })
    })
    //xu ly alias va luu file
    for await (let index of data){
        const fulllink = linkagency.concat(index.url)
        const po = fulllink.lastIndexOf('/')
        const alias = fulllink.slice(po+1)
        const browser3 = await puppeteer.launch();
        try {
            const page = await browser3.newPage();
            await page.goto(`${fulllink}`);
            await page.waitForTimeout(2000);
            let html_content = await page.evaluate(el => el.innerHTML, await page.$('#__nuxt'));
            if (fs.existsSync(`./data/${alias}.html`)){
                console.log(`ton tai data/${alias}.html`)
            }
            else{
                fs.writeFile(`./data/${alias}.html`, html_content, { flag: 'a+' }, err => {
                    if (err)
                        console.log("Error")
                    else {
                        console.log(`${alias}`)
                    }
                })
            }
            
        } catch (err) {
            console.log(err);
        }
        await browser3.close()
    }
    // dong xu ly alias va luu file
    await browser1.close()
    await browser.close()
}

getdata()

