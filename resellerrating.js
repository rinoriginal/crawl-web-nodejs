const puppeteer = require('puppeteer');
const fs = require('fs')
const website ='https://www.resellerratings.com'
async function getData(){
    try{

        const browser = await puppeteer.launch({
            headless: false,
            defaultViewport: null
        })
    
        const page = await browser.newPage()
    
        const url = 'https://www.resellerratings.com/sitemap'
        await page.goto(url, {waitUntil: 'networkidle2'})
        await page.waitForSelector('.sitemap')
        
        const results = await page.$$eval('div.sitemap > section:nth-child(2) > ul:nth-child(7) > li ', rows => {
            return rows.map(row => {
                const properties = {}
                const titleElement = row.querySelector('div.sitemap > section:nth-child(2) > ul:nth-child(7) > li > a')
                properties.url = titleElement.getAttribute('href')
                return properties
            })
        })
        // console.log(results.length)
        for await (let index of results){
            const browser1 = await puppeteer.launch({
                headless: false,
                defaultViewport: null
            })
        
            const page1 = await browser1.newPage()
            await page1.goto(index.url)
            await page1.waitForSelector('.grid-container')
            
            const result1s = await page1.$$eval('.grid-container > section:nth-child(1) > div > div.moreLink', row1s => {
                return row1s.map(row1 => {
                    const properties1 = {}
                    const titleElement = row1.querySelector('div.grid-container > section:nth-child(1) > div > div.moreLink  > a')
                    properties1.url = titleElement.getAttribute('href')
                    return properties1
                })
            })
            for await (let index1 of result1s){
                const fulllink = website.concat(index1.url)
                try {
                    const extractPartners = async url => {
                        const page = await browser.newPage()
                        await page.goto(url,{waitUntil: 'networkidle2'})
                        console.log(`scraping: ${url}`)
                        await page.waitForSelector('.paginate-pages')
                        //
                        const partnersOnPage = await page.$$eval('div.modules_library-site-search-page-stores > div.modules_library-site-store-card > h2', row1s => {
                            return row1s.map(row1 => {
                                const properties1 = {}
                                const titleElement = row1.querySelector('div.modules_library-site-search-page-stores > div.modules_library-site-store-card > h2 > a')
                                properties1.url = titleElement.getAttribute('href')
                                return properties1
                            })
                        })
                        //
                        for await (let index1 of partnersOnPage){
                            const fulllink1 = website.concat(index1.url)
                            const po = fulllink1.lastIndexOf('/')
                            const alias = fulllink1.slice(po+1)
                            const browser3 = await puppeteer.launch();
                            try {
                                const page = await browser3.newPage();
                                await page.goto(`${fulllink1}`);
                                await page.waitForTimeout(2000);
                                let html_content = await page.evaluate(el => el.innerHTML, await page.$('.container'));
                                if (fs.existsSync(`./data/${alias}.html`)){
                                    console.log(`ton tai data/${alias}.html`)
                                }
                                else{
                                    fs.writeFile(`./data/${alias}.html`, html_content, { flag: 'a+' }, err => {
                                        if (err)
                                            console.log("Error")
                                        else {
                                            console.log(`${alias}`)
                                        }
                                    })
                                }
                                
                            } catch (err) {
                                console.log(err);
                            }
                            await browser3.close()
                        }
                        //
                        await page.close()
                        if(partnersOnPage.length<1){
                            return partnersOnPage
                        }else{
                            const nextPageNumber = parseInt(url.match(/page=(\d+)$/)[1],10)+1
                            const nexturl = `${fulllink}&page=${nextPageNumber}`
                            return partnersOnPage.concat(await extractPartners(nexturl))
                            
                        }
                        //
                    }
                
                    const browser = await puppeteer.launch({
                        headless: false,
                        defaultViewport: null
                    })
                
                    
                    const firsturl = `${fulllink}&page=1`
                    const partners = await extractPartners(firsturl)
                    
                   
                    await browser.close()
                } catch (error) {
                    console.log(error)
                }
            }
            browser1.close()
        }
        browser.close()
    }
    catch(err){
        console.log(err)
    }
}

getData()

// xu ly link 
