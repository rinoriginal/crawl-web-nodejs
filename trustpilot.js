const cheerio = require('cheerio')
const fs = require('fs')

const axios = require('axios')
const walkSync = require("walk-sync")
const path = require('path')

async function getdata(){
    const paths = walkSync("./xml", {globs: ["**/*.xml"]});
    for await (let index1 of paths){
        console.log(index1)
        const data1 = fs.readFileSync(`./xml/${index1}`,{encoding: 'utf8'})
        const $ = cheerio.load(data1)

        const elSelector = $('url > loc')
        for await (let index2 of elSelector){
            const links = index2.children[0].data
            const po = links.lastIndexOf('/')
            const alias = links.slice(po+1) 
            await axios.get(links).then(async (urlResponse) => {
                const $ = cheerio.load(urlResponse.data)
                const dataweb1 = $.html()
                if (fs.existsSync(`./data/${alias}.html`)){
                    console.log(`ton tai .data/${alias}.html`)
                }
                else{
                    fs.writeFile(`./data/${alias}.html`, dataweb1, { flag: 'a+' }, err => {
                        if (err)
                            console.log("Error")
                        else {
                            console.log(`${alias}`)
                        }
                    })
                }
            }).catch(()=>{})
        }
    }
}

getdata()
