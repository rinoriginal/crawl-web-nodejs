const cheerio = require('cheerio')
const axios = require('axios')
const fs = require('fs')
const website = 'https://en.softonic.com/'
const newapps = 'body > div.page > div:nth-child(2) > div > div > div > div:nth-child(6) > a'
let date ='body > div.page > div:nth-child(2) > div > div > div > div.row.mb-xl > div:nth-child(1) > section > div > a'
let trending= 'body > div.page > div:nth-child(2) > div > div > div > div:nth-child(8) > a'
let weeklydownload ='body > div.page > div:nth-child(2) > div > div > aside > section.list-top-apps.mb-l > div > a'
let newversions ='body > div.page > div:nth-child(2) > div > div > div > div.row.mb-xl > div:nth-child(3) > section > div > a'
let lastnews ='body > div.page > div:nth-child(2) > div > div > div > div.row.mb-xl > div:nth-child(2) > section > div > a'
let arraylist=[newapps,date,trending,weeklydownload,newversions,lastnews]

async function GetCategories(){     
    await axios.get(website).then(async(urlResponse) =>{
        const $ = cheerio.load(urlResponse.data)
        const array = $("body > div.page > div:nth-child(4) > div > div > div > section > div > label > span")
        for await (let index of array) {
            const a = $(index).text()
            const aliasWeb=a.toLowerCase()
            // console.log(aliasWeb)
            link=`https://en.softonic.com/${aliasWeb}`
            await axios.get(link).then(async(urlCate) => {
                const $ = cheerio.load(urlCate.data)
                const array1 = $("body > div.page > div:nth-child(2) > div > div > div > section > ul.row > li > div.list-category-apps > a")
                for await (let index1 of array1){
                    const aliasCate= index1.attribs.href     
                    const aliasCategory = aliasCate.replace(`${link}/`,'')
                    // console.log(aliasCategory)
                    await axios.get(aliasCate).then(async(urlRes) =>{
                        const $ = cheerio.load(urlRes.data)
                        for (k=0;k<arraylist.length;k++){    
                            const array2 = $(arraylist[k])
                            for await (let index2 of array2){
                                const linkbrowsers = index2.attribs.href                                            
                                const aliasf = linkbrowsers.replace(`${aliasCate}:`,'')
                                // console.log(aliasf)
                                await axios.get(linkbrowsers).then(async(urlLink) => {
                                    const $ = cheerio.load(urlLink.data)
                                    const array3 = $("div.content > nav.pagination-links > a")
                                    for await(let index3 of array3){
                                        const linknew = index3.attribs.href
                                        const n = linknew.replace(`${linkbrowsers}/`,'')
                                        const relink = linknew.replace(`/${n}`,'')
                                        for(i=1;i<=n; i++){
                                           //link category
                                           if(i==1){
                                               linkdb = `${relink}`      
                                           }else{
                                               linkdb = `${relink}/${i}`      
                                           }
                                           await axios.get(linkdb).then(async(urlResp) =>{   
                                               const $ = cheerio.load(urlResp.data)
                                               const array4 = $("div.content > ul > li > a")
                                               for await (let index4 of array4){
                                                   const dtlink = index4.attribs.href                  
                                                   const a = dtlink.replace('https://','');
                                                    const position = a.indexOf('.')
                                                    const b = a.slice(position)
                                                    const aliasd = a.replace(b,'')
                                                    // console.log(dtlink)
                                                    await axios.get(dtlink).then((urlres1) => {                                                       
                                                            const $ = cheerio.load(urlres1.data)
                                                            const dataweb = $.html()
                                                            if (fs.existsSync(`./${aliasWeb}/${aliasCategory}/${aliasf}/${aliasd}.html`)){
                                                                console.log(`ton tai ${aliasWeb}/${aliasCategory}/${aliasf}/${aliasd}.html`)
                                                            }
                                                            else{
                                                                fs.writeFile(`./${aliasWeb}/${aliasCategory}/${aliasf}/${aliasd}.html`, dataweb, { flag: 'a+' }, err => {
                                                                    if (err)
                                                                        console.log("Error")
                                                                    else {
                                                                        console.log(`${aliasd}`)
                                                                    }
                                                                })
                                                            }
                                                        }).catch(()=>{})

                                               }  
                                           }).catch(()=>{})
                                           
                                                
                                        }
                                    } 
                                }).catch(()=>{})         
                            }                      
                        }
                    }).catch(()=>{})
                }
                 
            }).catch(()=>{})
        }
        
    })
}

GetCategories()