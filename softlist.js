const fs = require('fs')
const cheerio = require('cheerio')
const axios = require('axios')
const puppeteer = require('puppeteer')
const walkSync = require("walk-sync")
const path = require('path')

async function getdata(){
    const paths = walkSync("./xml", {globs: ["**/*.xml"]})
    for await (let index of paths){
        console.log(index)
        const data = fs.readFileSync(`./xml/${index}`, {encoding: 'utf8'})
        const $ = cheerio.load(data)

        const elSelector = $('urlset > url > loc')
        for await (let index1 of elSelector){
            const links = index1.children[0].data
            const po = links.lastIndexOf('/')
            const alias = links.slice(po+1)
            await axios.get(links).then(async (urlResponsive) => {
                const $ = cheerio.load(urlResponsive.data)
                const dataweb = $.html()
                if (fs.existsSync(`./data/${alias}.html`)){
                    console.log(`ton tai .data/${alias}.html`)
                }
                else{
                    fs.writeFile(`./data/${alias}.html`, dataweb, { flag: 'a+' }, err => {
                        if (err)
                            console.log("Error")
                        else {
                            console.log(`${alias}`)
                        }
                    })
                }
            }).catch(()=>{})
        }
    }
}

getdata()