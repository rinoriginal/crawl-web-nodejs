const cheerio = require('cheerio')
const fs = require('fs')
const axios = require('axios')
const website = 'https://www.sitejabber.com'
const a = website.indexOf('.')
const b = website.slice(a+1)
const c = b.indexOf('.')
const d = b.slice(c)
const aliasWeb = b.replace(d,'')

async function GetData(){
    await axios.get(website).then(async (urlResponsive) => {
        const $ = cheerio.load(urlResponsive.data)
        const array = $('#homepage > div:nth-child(8) > div > a')
        for await (let index of array)
        {
            const btnlink = website.concat(index.attribs.href) 
            // console.log(btnlink)
            await axios.get(btnlink).then(async (urlRes) => {
                const $ = cheerio.load(urlRes.data)
                const array1 = $(' ul.categories-browse__list > li.categories-browse__list__item > a.categories-browse__link')
                
                for await (let index1 of array1){
                    if(index1.attribs.href != '#'){
                        const link = website.concat(index1.attribs.href) // 855
                         await axios.get(link).then(async (urllink) => {
                            const $ = cheerio.load(urllink.data)
                            const array2 = $('#view_category > div.categories-view__heading > div > div > a:nth-child(2)')
                            const aliasCategory = array2.text()
                            //  div.url-list__item__content > h2 > a
                            const array3 = $('#all-sites-section > div > div.categories-view__flex > div.categories-view__right > div.pagination > div.pagination__numbers > span > a')
                            if(array3.text()==''){
                                xpage = 1
                                linkdb = link
                                
                                await axios.get(linkdb).then(async(urlResp) =>{   
                                    const $ = cheerio.load(urlResp.data)
                                    const array5 = $('div.url-list__item__content > h2 > a')
                                    for await (let index4 of array5){
                                        const dlink = index4.attribs.href
                                        const po = dlink.lastIndexOf('/')
                                        const re = dlink.slice(po+1)
                                        const po1 = re.indexOf('.')
                                        const re1 = re.slice(po1)
                                        const alias = re.replace(re1,'')
                                        const dtlink = website.concat(dlink)
                                        await axios.get(dtlink).then((urlres1) => {                                                       
                                            const $ = cheerio.load(urlres1.data)
                                            const dataweb = $.html()
                                            if (fs.existsSync(`./${aliasWeb}/${aliasCategory}/${alias}.html`)){
                                                console.log(`ton tai ${aliasWeb}/${aliasCategory}/${alias}.html`)
                                            }
                                            else{
                                                fs.writeFile(`./${aliasWeb}/${aliasCategory}/${alias}.html`, dataweb, { flag: 'a+' }, err => {
                                                    if (err)
                                                        console.log("Error")
                                                    else {
                                                        console.log(`${alias}`)
                                                    }
                                                })
                                            }
                                        }).catch(()=>{})
                                    }
                                    
                                }).catch(()=>{})
                            }
                            else{
                                 xpage = array3.text()
                                 const n = xpage[xpage.length-1]
                                 const array4 = array3.slice(-1)
                                 for await (let index3 of array4){
                                     const linkpage = index3.attribs.href
                                     // console.log(linkpage)
                                     const relink = linkpage.replace(n,' ')
                                     for(i=1;i<=n;i++){
                                         if(i==1){
                                             linkdb = link
                                         }else{
                                             linkdb = relink.replace(' ',i)
                                         }
                                    
                                         await axios.get(linkdb).then(async(urlResp) =>{   
                                             const $ = cheerio.load(urlResp.data)
                                             const array5 = $('div.url-list__item__content > h2 > a')
                                             for await (let index4 of array5){
                                                 const dlink = index4.attribs.href
                                                 const po = dlink.lastIndexOf('/')
                                                 const re = dlink.slice(po+1)
                                                 const po1 = re.indexOf('.')
                                                 const re1 = re.slice(po1)
                                                 const alias = re.replace(re1,'')
                                                 const dtlink = website.concat(dlink)
                                                 await axios.get(dtlink).then((urlres1) => {                                                       
                                                     const $ = cheerio.load(urlres1.data)
                                                     const dataweb = $.html()
                                                     if (fs.existsSync(`./${aliasWeb}/${aliasCategory}/${alias}.html`)){
                                                         console.log(`ton tai ${aliasWeb}/${aliasCategory}/${alias}.html`)
                                                     }
                                                     else{
                                                         fs.writeFile(`./${aliasWeb}/${aliasCategory}/${alias}.html`, dataweb, { flag: 'a+' }, err => {
                                                             if (err)
                                                                 console.log("Error")
                                                             else {
                                                                 console.log(`${alias}`)
                                                             }
                                                         })
                                                     }
                                                 }).catch(()=>{})
                                             }
                                             
                                         }).catch(()=>{})
                                     } 
                            }
                            }                    
                         }).catch(()=>{})
                    }
                }
                
                    
                
                
            }).catch(()=>{}) 
        }
    }).catch(()=>{})
}

GetData()