const puppeteer = require('puppeteer')
const fs = require('fs')
const website = 'https://www.yelp.com/'
async function getdata(){
    try {
        const browser = await puppeteer.launch({ headless: false })
        const page = await browser.newPage()
        await page.goto(website)
        await page.click('#page-content > div:nth-child(3) > div > div > yelp-react-root > div > div > div:nth-child(2) > div:nth-child(2) > div > div:nth-child(4) > a')
        await page.waitForTimeout(2000)
        await page.waitForSelector('div.u-text-truncate__a5c5a__2cKiU.js-browse-businesses_category')
        await page.waitForTimeout(2000)
        //16 link duoi
        const links1 = await page.$$eval('div.u-text-truncate__a5c5a__2cKiU.js-browse-businesses_category > a', rows => {
            return rows.map(row => {
                const properties = {}
                properties.url = row.getAttribute('href')
                return properties
            })
        })
        //8 link tren
        const links2 = await page.$$eval('div.arrange-unit__a5c5a__3XPkE > a.category-tile__a5c5a__3wmQc', rows => {
            return rows.map(row => {
                const properties = {}
                properties.url = row.getAttribute('href')
                return properties
            })
        })
        console.log("==================================")
        // xu ly 8 link tren
        for await (let index of links2){
            const fulllink = website.concat(index.url)
            try {
                const extractPartners = async url => {
                    const page = await browser.newPage()
                    await page.goto(url)
                    console.log(`scraping: ${url}`)
                    const partnersOnPage = await page.$$eval('#main-content > div > ul > li > div > div > div > div.arrange-unit__09f24__rqHTg.arrange-unit-fill__09f24__CUubG.border-color--default__09f24__NPAKY > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div > div > h4 > span > a', row1s => {
                        return row1s.map(row1 => {
                            const properties1 = {}
                            properties1.url = row1.getAttribute('href')
                            return properties1
                        })
                    })
                    
                    for await (let index1 of partnersOnPage){
                        const fulllink1 = website.concat(index1.url)
                        const po = fulllink1.lastIndexOf('/')
                        const alias = fulllink1.slice(po+1)
                        const browser3 = await puppeteer.launch();
                        try {
                            const page = await browser3.newPage();
                            await page.setViewport({ width: 1920, height: 1080 });
                            await page.setRequestInterception(true);
                            page.on('request', (req) => {
                            if(req.resourceType() === 'image' || req.resourceType() == 'stylesheet' || req.resourceType() == 'font'){
                                req.abort();
                            }
                            else {
                                req.continue();
                            }
                            })
                            await page.goto(`${fulllink1}`);
                            await page.waitForTimeout(2000);
                            let html_content = await page.evaluate(el => el.innerHTML, await page.$('body > yelp-react-root > div:nth-child(1)'));
                            if (fs.existsSync(`./data/${alias}.html`)){
                                console.log(`ton tai data/${alias}.html`)
                            }
                            else{
                                fs.writeFile(`./data/${alias}.html`, html_content, { flag: 'a+' }, err => {
                                    if (err)
                                        console.log("Error")
                                    else {
                                        console.log(`${alias}`)
                                    }
                                })
                            }
                            
                        } catch (err) {
                            console.log(err);
                        }
                        await browser3.close()
                    }
                    await page.close()
                    if(partnersOnPage.length<1){
                        return partnersOnPage
                    }else{
                        const nextPageNumber = parseInt(url.match(/start=(\d+)$/)[1],10)+10
                        const nexturl = `${fulllink}&start=${nextPageNumber}`
                        return partnersOnPage.concat(await extractPartners(nexturl))
                        
                    } 
                }
            
                const browser = await puppeteer.launch()
                const firsturl = `${fulllink}&start=00`
                const partners = await extractPartners(firsturl)
            } catch (error) {
                console.log(error)
            }
        }
        // xu ly 16 link duoi
        for await (let index1 of links1){
            const fulllink1 = website.concat(index1.url)
            const extractPartners = async url => {
                const page = await browser.newPage()
                await page.goto(url)
                console.log(`scraping: ${url}`)
                const partnersOnPage = await page.$$eval('h3 > span > a', row1s => {
                    return row1s.map(row1 => {
                        const properties1 = {}
                        properties1.url = row1.getAttribute('href')
                        return properties1
                    })
                })
                
                for await (let index1 of partnersOnPage){
                    const fulllink1 = website.concat(index1.url)
                    const po = fulllink1.lastIndexOf('/')
                    const alias = fulllink1.slice(po+1)
                    const browser3 = await puppeteer.launch();
                    try {
                        const page = await browser3.newPage();
                        await page.setViewport({ width: 1920, height: 1080 });
                        await page.setRequestInterception(true);
                        page.on('request', (req) => {
                        if(req.resourceType() === 'image' || req.resourceType() == 'stylesheet' || req.resourceType() == 'font'){
                            req.abort();
                        }
                        else {
                            req.continue();
                        }
                        })
                        await page.goto(`${fulllink1}`);
                        await page.waitForTimeout(2000);
                        let html_content = await page.evaluate(el => el.innerHTML, await page.$('body > yelp-react-root > div:nth-child(1)'));
                        if (fs.existsSync(`./data/${alias}.html`)){
                            console.log(`ton tai data/${alias}.html`)
                        }
                        else{
                            fs.writeFile(`./data/${alias}.html`, html_content, { flag: 'a+' }, err => {
                                if (err)
                                    console.log("Error")
                                else {
                                    console.log(`${alias}`)
                                }
                            })
                        }
                        
                    } catch (err) {
                        console.log(err);
                    }
                    await browser3.close()
                }
                await page.close()
                if(partnersOnPage.length<1){
                    return partnersOnPage
                }else{
                    const nextPageNumber = parseInt(url.match(/start=(\d+)$/)[1],10)+10
                    const nexturl = `${fulllink}&start=${nextPageNumber}`
                    return partnersOnPage.concat(await extractPartners(nexturl))
                    
                } 
            }
        
            const browser = await puppeteer.launch()
            const firsturl = `${fulllink}&start=00`
            const partners = await extractPartners(firsturl)
        }
        await browser.close()
    } catch (error) {
        console.log(error)
    }
    
}

getdata()
